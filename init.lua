-- For Neovide (GUI)
if vim.g.neovide then
    vim.o.guifont = "Source Han Code JP:h7"
    vim.g.neovide_cursor_animation_length = 0
    vim.g.neovide_hide_mouse_when_typing = true
    vim.g.neovide_remember_window_size = true

    -- padding
    vim.g.neovide_padding_top = 2
    vim.g.neovide_padding_bottom = 2
    vim.g.neovide_padding_left = 8
    vim.g.neovide_padding_right = 8

end




-- Basic Settings

vim.cmd([[
    if exists('+termguicolors')
        set termguicolors
    endif
]])

vim.cmd([[
    try
        colorscheme yah
    catch
        colorscheme desert
    endtry
]])

vim.cmd(":filetype plugin indent on")
vim.cmd(":syntax enable")

vim.cmd("set clipboard+=unnamedplus")

vim.g.mapleader = " "
vim.opt.incsearch = true
vim.opt.smartcase = true
vim.opt.wrapscan = true
vim.opt.gdefault = true

vim.opt.encoding = "utf-8"
vim.opt.fenc = "utf-8"

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.ruler = true
vim.opt.wildmenu = true

vim.opt.showcmd = true
vim.opt.showmatch = true

vim.opt.ambiwidth = "double"

vim.opt.laststatus = 2
vim.opt.cmdheight = 3

vim.opt.list = true
vim.opt.listchars = "tab:>>,trail:_,eol:$,extends:>,precedes:<,nbsp:%"

vim.opt.backspace = "indent,eol,start"
vim.opt.whichwrap = "b,s,h,l,<,>,[,]"

vim.opt.scrolloff = 4
vim.opt.sidescrolloff = 16
vim.opt.sidescroll = 1

vim.opt.autoread = true
vim.opt.hidden = true
vim.opt.swapfile = false
vim.opt.writebackup = false
vim.opt.backup = false

vim.opt.shellslash = true

vim.opt.smarttab = true

vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4




-- Keymap

vim.keymap.set('n', '<leader><Escape>', ":nohl<CR>", {silent = true})
vim.keymap.set('n', '[b', ":bp<CR>", {silent = true})
vim.keymap.set('n', ']b', ":bn<CR>", {silent = true})
vim.keymap.set('n', '<leader>q', ":bd<CR>", {silent = true})




-- Must install `packer.nvim`
--
--   For Windows (use `PowerShell`)
--     git clone https://github.com/wbthomason/packer.nvim \
--     "$env:LOCALAPPDATA\nvim-data\site\pack\packer\start\packer.nvim"
--
--   For Unix/Linux:
--     git clone --depth 1 https://github.com/wbthomason/packer.nvim \
--     ~/.local/share/nvim/site/pack/packer/start/packer.nvim

return require('packer').startup(function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- My own colorscheme
    use {'inkch/vim-yah', config = 'vim.cmd[[colorscheme yah]]'}

    use {'ojroques/nvim-hardline', config = function()
        require('hardline').setup {}
    end}

    use {'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim',
        config = function()
            require('neogit').setup {
                disable_commit_confirmation = true,
                use_magit_keybindings = true,
            }
            vim.keymap.set('n', '<leader>gg', ":Neogit<CR>", {silent = true})
        end
    }

    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.0',
        requires = {{'nvim-lua/plenary.nvim'}},
        config = function()
            local b = require('telescope.builtin')
            vim.keymap.set('n', '<leader>,.', b.find_files, {})
            vim.keymap.set('n', '<leader>,/', b.live_grep, {})
            vim.keymap.set('n', '<leader>,,', b.buffers, {})
            vim.keymap.set('n', '<leader>,;', b.command_history, {})
            vim.keymap.set('n', '<leader>,?', b.search_history, {})
        end
    }

    use {
        "kylechui/nvim-surround",
        tag = "*", -- Use for stability; omit to use `main` branch for the latest features
        config = function()
            require("nvim-surround").setup{}
        end
    }

    use {'NvChad/nvim-colorizer.lua', config = function()
            require('colorizer').setup {}
        end
    }

    use {'terrortylor/nvim-comment',
        config = function()
            require('nvim_comment').setup({comment_empty = false})
        end
    }

    use {'nvim-treesitter/nvim-treesitter'}

    use {'nvim-orgmode/orgmode', config = function ()
            require('orgmode').setup_ts_grammar()
            require('nvim-treesitter.configs').setup {
                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = {'org'},
                },
                ensure_installed = {'org'},
            }
            require('orgmode').setup {}
        end
    }
end)
